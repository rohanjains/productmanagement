<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
table {
	border-collapse: collapse;
}

table, td, th {
	border: 1px solid black;
}
</style>

</head>
<body>

<h2>Category Page</h2>

<br><br>

<div>
Category Supplements
</div>

<br><br>


<div>
Category Dance Therapy 2
</div>

<h2>${test}</h2>
<h2>${cat}</h2>
<h2>${cat.name}</h2>

<div id="categories">

<table>
<th>CategoryName</th>
<th>CategoryDesc</th>

<c:forEach items="${categories}" var="category">
	<tr height=50px>
		<td width="300px" align="center">
			 <a href="/productByCatid/${category.id}">${category.name}</a> 
		</td>
		<br>
		<td width="500px" align="center">
			${category.description} 
		</td>
	</tr>
</c:forEach>
</table>


</div>

</body>
</html>