<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>

<script>

$( document ).ready(function() {
			 $("#submit").click(function()  {
				 
				    if(!(document.getElementById('mobileno').value.length === 10)
						 || !/^\d+$/.test(document.getElementById('mobileno').value)) {
				  		  alert("MobileNo must be 10 digits");
				  		  return false;
					}

				    var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
					var emailIdGiven = document.getElementById('email').value;
				    if(!emailIdGiven.match(reEmail)) {
				      alert("Invalid email address");
				      return false;
				    }

				    var passwordGiven = document.getElementById('password').value;
				    var passwordExpr = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
				    if(!passwordGiven.match(passwordExpr)) {
					      alert("Invalid password");
					      return false;
					    }
					//mobile no only digits
					//email id has @
					return true;
			});

				
		});
</script>

<meta charset="UTF-8">
<title>Insert title here</title>

</head>
<body>

<h2>Register</h2>

<form:form action="registrationcontroller" method="POST" modelAttribute="userdetails">
<table>
    <tr>
        <td><form:label path="firstname">firstname: </form:label></td> 
        <td> <form:input path="firstname" class="formInput" required="required"/> </td>
    </tr>
    <tr>
        <td><form:label path="lastname">lastname: </form:label></td> 
        <td> <form:input path="lastname" class="formInput" required="required"/> </td>
    </tr>
    <tr>
        <td><form:label path="mobileno">mobileno: </form:label></td> 
        <td> <form:input type="number" id="mobileno" path="mobileno" class="formInput" required="required"/> </td>
    </tr>
    <tr>
        <td><form:label path="email">email: </form:label></td> 
        <td> <form:input id="email" path="email" class="formInput" required="required"/> </td>
    </tr>
    <tr>
        <td><form:label path="dob">dob: </form:label></td> 
        <td> <form:input type="date" required="required" path="dob"/> </td>
    </tr>
    <tr>
        <td><form:label path="gender">gender: </form:label></td> 
        <td> <form:input required="required" path="gender"/> </td>
    </tr>
    <tr>
        <td><form:label path="address">address: </form:label></td> 
        <td> <form:input path="address" class="formInput" required="required"/> </td>
    </tr>
    <tr>
        <td><form:label path="expectation">expectation: </form:label></td> 
        <td width="200%"> <form:input path="expectation" class="formInput" required="required"/> </td>
    </tr>
    <tr>    
        <td><form:label path="password">password: </form:label></td> 
        <td> <form:input type="password" path="password" class="formInput" required="required"/> </td>
    </tr>
    <tr>
        <td><input id="submit" type="submit" class="sendButton" value="Register"/></td>
        <td><a href="/" >Back</a></td>
    </tr>
    
    
</table>
</form:form>

<br>



<h3>${errorMesaage} </h3>

</body>
</html>
