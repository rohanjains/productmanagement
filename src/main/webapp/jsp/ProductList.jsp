<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ProductList</title>

<style>
.productsTable {
	border: 1px solid black;
	border-collapse: collapse;
}

th, tr, td {

	border: 1px solid black;
}
</style>

</head>
<body>

<h1>List of productss</h1>

<table class="productsTable">
	<th>
		productName
	</th>
	<th>
		productDesc
	</th>
	
	<c:forEach items="${products}" var="product" >
	<tr>
		<td>
			${product.name}
		</td>
		<td>
			${product.description}
		</td>
	</tr>
	</c:forEach>
	

</table>

</body>
</html>