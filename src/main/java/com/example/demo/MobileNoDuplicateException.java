package com.example.demo;

public class MobileNoDuplicateException extends Exception{

	public MobileNoDuplicateException(String string) {
		super(string);
	}
	
}
