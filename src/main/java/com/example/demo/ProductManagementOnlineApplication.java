package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


//@ComponentScan(basePackageClasses = {com.example.demo.UserDetailsRepository.class})
@SpringBootApplication
@ComponentScan(basePackages = {"com.example.demo.*", "com.example.*"})
public class ProductManagementOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductManagementOnlineApplication.class, args);
	}

}
