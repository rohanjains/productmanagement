package com.example.demo;

public class RegistrationStatus {
	boolean status;
	String message;
	
	public boolean getStatus() {
		return status;
	}
	
	public RegistrationStatus(boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
