package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

	@Autowired
	CategoryRepository categoryRep;
	
	@ResponseBody
	@GetMapping("/test")
	public String helloTest() {
		return "test";
	}
	
	
	@GetMapping("/")
	public String getHomepage() {
		return "home";
	}
	
	@GetMapping("/aboutUs")
	public String getAboutUsPage() {
		return "aboutUs";
	}

	@GetMapping("/contactUs")
	public String getContactUsPage() {
		return "contactUs";
	}

	
	@ResponseBody
	@GetMapping("/hometest")
	public String getHomepageTest() {
		return "hometest";
	}
	
//	@ResponseBody
	@GetMapping("/category")
	public String getCategoryPage(ModelMap map) {

		List<Category> categories = categoryRep.findAll();
		
		map.addAttribute("categories", categories);
		
		return "category";
	}
	
	
	@Autowired
	ProductRepository productRep;
	
	@GetMapping("/productByCatid/{categoryid}")
	public String getProductByCatid(@PathVariable int categoryid, ModelMap map) {

//		List<Category> categories = categoryRep.findAll();
//		
//		map.addAttribute("categories", categories);
//		
//		return "category";
		
		map.addAttribute("products", productRep.findByCategoryid(categoryid));
		
		return "ProductList";
	}
	
	@ResponseBody
	@GetMapping("/productByCatid2/{categoryid}")
	public List<Product> getProductByCatid2(@PathVariable int categoryid, ModelMap map) {

//		List<Category> categories = categoryRep.findAll();
//		
//		map.addAttribute("categories", categories);
//		
//		return "category";
		
		return productRep.findByCategoryid(categoryid);
	}
	
	
}
