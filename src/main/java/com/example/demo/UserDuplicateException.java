package com.example.demo;

public class UserDuplicateException extends Exception{

	public UserDuplicateException(String string) {
		super(string);
	}
	
}
