package com.example.demo;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController {
	
	@Autowired
	RegistrationService registrationService;
	
	@ResponseBody
	@PostMapping("/registration")
	public ResponseEntity<String> register(@RequestBody UserDetails userdetails) {
		
		Logger logger = Logger.getLogger(RegistrationService.class);
		BasicConfigurator.configure();
		
		boolean status=false;
		try {
			status = registrationService.register(userdetails);
			if(status) {
				logger.info("successfully registered user with emailid: " + userdetails.getEmail());
				return ResponseEntity.created(null).body("successfully Registered");
			} else {
				return ResponseEntity.badRequest().body("error registering user");
			}
			
		} catch (MobileNoDuplicateException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (UserDuplicateException e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body("Internal Server error. user creation failed " + e.getMessage());
		}
		
	}
	
	

	
	@ResponseBody
	@PostMapping("/userdet")
	public UserDetails userdetails() {
		UserDetails userDet = new UserDetails();
		userDet.setAddress("testAddress");
		return userDet;
	}
	
	@ResponseBody
	@GetMapping("/hello")
	public String hello() {
		return "hello";
	}
	

	
	@PostMapping("/registrationcontroller")
	public String registrationcontroller(@ModelAttribute("userdetails")UserDetails userDetails, ModelMap map) {
		System.out.println("test");
		
		try {
			registrationService.register(userDetails);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("errorMesaage", e.getMessage());
			return "registration";
		}
		
		
//		map.put("errorMesaage", "No Error");
		return "registration";
	}
	
	@PostMapping("/registrationcontroller2")
	public String registrationcontroller2(@RequestBody UserDetails userDetails) throws Exception {
		System.out.println("test");
		registrationService.register(userDetails);
		System.out.println("hello WOrld");
		return "hello";
	}
	
	
	
	@GetMapping("/register")
	public ModelAndView register() {
		return new ModelAndView("registration", "userdetails", new UserDetails());
	}
	
}
