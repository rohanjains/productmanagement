package com.example.demo;

import java.util.Optional;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

	@Autowired
	UserDetailsRepository userDetRep;

	@Autowired
	LoginDetailsRepository loginRep;

	@Autowired
	LoginDetails loginDetails;

	public boolean register(UserDetails userdetails) throws Exception {

		loginDetails.setIsactive(true);
		loginDetails.setPassword(userdetails.getPassword());
		loginDetails.setRole("USER");
		loginDetails.setIsactive(true);
		loginDetails.setUsername(userdetails.getEmail());
		loginRep.save(loginDetails);

		Optional<UserDetails> userInDbOpt = userDetRep.findById(userdetails.getEmail());
		if (userInDbOpt.isPresent()) {
			throw new UserDuplicateException("User Already Exist");
		}
		UserDetails userMobInDb = userDetRep.findByMobileNumber(userdetails.getMobileno());
		if (userMobInDb != null) {
			throw new MobileNoDuplicateException("User MobileNo in DB");
		}
		userDetRep.save(userdetails);

		return true;
	}

}
