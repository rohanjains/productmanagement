package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, String>{
	
	@Query(value="SELECT * FROM userdetails WHERE mobileno = ?1",nativeQuery=true)
	UserDetails findByMobileNumber(String mobileNo);
}
